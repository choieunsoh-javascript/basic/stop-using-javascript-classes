// * Class Style
class Book {
  author;
  title;
  readCount;

  constructor(authorName, bookTitle) {
    this.author = authorName;
    this.title = bookTitle;
    this.readCount = 0;
  }

  incrementReadCount() {
    this.readCount++;
  }

  read() {
    console.log("This is a good book!");
    this.incrementReadCount();
  }

  getReadCount() {
    return this.readCount;
  }
}

const myFirstBook = new Book("Jack Beaton", "Getting To Mars");
myFirstBook.getReadCount(); // 0
myFirstBook.read(); // 'This is a good book!'
myFirstBook.incrementReadCount(); // calling private methods outside the class won't work in strict OOP languages
myFirstBook.read(); // 'This is a good book!'
myFirstBook.readCount; // directly accessing a class's private fields won't work in strict OOP languages
myFirstBook.getReadCount(); // 2

// * Module Style
const createBook = (authorName, bookTitle) => {
  const author = authorName;
  const title = bookTitle;
  let readCount = 0;

  const incrementReadCount = () => readCount++;

  const read = () => {
    console.log("This is a good book!");
    incrementReadCount();
  };

  const getReadCount = () => readCount;

  return {
    read,
    getReadCount,
  };
};

const mySecondBook = createBook("Gabriel Rumbaut", "Cats Are Better Than Dogs");
mySecondBook.getReadCount(); // 0
mySecondBook.read(); // 'This is a good book!'
//mySecondBook.incrementReadCount(); // will throw an error
mySecondBook.read(); // 'This is a good book!'
//mySecondBook.readCount; // will also throw an error
mySecondBook.getReadCount(); // 2
